package com.example.daftarkulinera114306;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class KulinerAdapter extends RecyclerView.Adapter<KulinerAdapter.ViewHolder> {
    private ArrayList<Kuliner> listKuliner;

    public KulinerAdapter(ArrayList<Kuliner> listKuliner){
        this.listKuliner = listKuliner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewHolder holder = new ViewHolder(inflater.inflate(R.layout.kuliner_item,
                                            parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Kuliner kuliner = listKuliner.get(position);

        holder.txtNama.setText(kuliner.nama);
        holder.txtAlamat.setText(kuliner.alamat);
        holder.imgGambar.setImageResource(kuliner.gambar);

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(holder.itemView.getContext(), kuliner.nama, Toast.LENGTH_LONG).show();
                Intent it = new Intent(holder.itemView.getContext(),
                                        DetailKulinerActivity.class);
                it.putExtra("kuliner", kuliner);
//                it.putExtra("nama_kuliner", kuliner.nama);
//                it.putExtra("img_kuliner", kuliner.gambar);

                holder.itemView.getContext().startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listKuliner.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNama, txtAlamat;
        ImageView imgGambar;
        ConstraintLayout itemLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.txt_nama);
            txtAlamat = itemView.findViewById(R.id.txt_alamat);
            imgGambar = itemView.findViewById(R.id.img_gambar);
            this.itemLayout = itemView.findViewById(R.id.item_layout);
        }
    }
}
