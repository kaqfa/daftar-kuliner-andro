package com.example.daftarkulinera114306;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailKulinerActivity extends AppCompatActivity {

    ImageView imgGambar;
    TextView txtNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kuliner);

        imgGambar = findViewById(R.id.img_gambar_detail);
        txtNama = findViewById(R.id.txt_nama_detail);

        Intent it = getIntent();
        Kuliner kuliner = (Kuliner) it.getSerializableExtra("kuliner");

        txtNama.setText(kuliner.nama);
        imgGambar.setImageResource(kuliner.gambar);
    }
}