package com.example.daftarkulinera114306;

import java.io.Serializable;

public class Kuliner implements Serializable {
    int gambar;
    String nama, alamat;

    public Kuliner(String nama, String alamat, int gambar){
        this.gambar = gambar;
        this.nama = nama;
        this.alamat = alamat;
    }
}
