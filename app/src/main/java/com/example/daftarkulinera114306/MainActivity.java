package com.example.daftarkulinera114306;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Kuliner> list_kuliner;
    RecyclerView recKuliner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recKuliner = findViewById(R.id.rec_kuliner);
        initData();

        recKuliner.setAdapter(new KulinerAdapter(list_kuliner));
        recKuliner.setLayoutManager(new LinearLayoutManager(this));
//        recKuliner.setLayoutManager(new GridLayoutManager(this, 2));
    }

    private void initData(){
        list_kuliner = new ArrayList<>();

        // koneksi DB
        // query
        // iterasi hasil query ke arraylist
        list_kuliner.add(new Kuliner("Soto Pak Man",
                "Jl. Pamularsih Semarang", R.drawable.soto));
        list_kuliner.add(new Kuliner("Waroeng Es Do",
                "Depan UDINUS Gd. A", R.drawable.angkringan));
        list_kuliner.add(new Kuliner("Ayam Goreng Mak Sri",
                "Jl. Indraprasta Semarang", R.drawable.ayam));
    }
}